#include <iostream>
#include <utility>

using std::string;
using std::cout;
using std::endl;

class Car {
private:
    string company;
    string model;
    int speed;
    double gasTank;
    int mileage;
public:
    Car(string _company, string _model, int _speed, double _gasTank, int _mileage) {
        company = std::move(_company);
        model = std::move(_model);
        speed = _speed;
        gasTank = _gasTank;
        mileage = _mileage;
    }

    void printData() {
        printf("[%s] %s\nSpeed: %d km/h\nGas Tank: %.1f litres\nMileage: %d km",
               company.c_str(), model.c_str(), speed, gasTank, mileage);
    }
};

class BMWCar: public Car {
public:
    BMWCar(string model, int speed, double gasTank, int mileage)
        : Car("BMW", std::move(model), speed, gasTank, mileage) {}
};


int main() {
    Car c1 {"BMW", "X5", 180, 10, 0};

    c1.printData();

    BMWCar c2 {"X6", 190, 10, 1000};

    c2.printData();

    return 0;
}
